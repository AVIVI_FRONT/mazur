'use strict';
if(!window.console) window.console = {};
if(!window.console.memory) window.console.memory = function() {};
if(!window.console.debug) window.console.debug = function() {};
if(!window.console.error) window.console.error = function() {};
if(!window.console.info) window.console.info = function() {};
if(!window.console.log) window.console.log = function() {};

// sticky footer
//-----------------------------------------------------------------------------
if(!Modernizr.flexbox) {
    (function() {
        var
            $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            noFlexboxStickyFooter = function() {
                $pageBody.height('auto');
                if($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
                    $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
                } else {
                    $pageWrapper.height('auto');
                }
            };
        $(window).on('load resize', noFlexboxStickyFooter);
    })();
}
if(ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
    (function(){
        var
            $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            ieFlexboxFix = function() {
                if($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
                    $pageWrapper.height($(window).height());
                    $pageBody.removeClass('flex-none');
                } else {
                    $pageWrapper.height('auto');
                }
            };
        ieFlexboxFix();
        $(window).on('load resize', ieFlexboxFix);
    })();
}

$(function() {

// placeholder
//-----------------------------------------------------------------------------
    $('input[placeholder], textarea[placeholder]').placeholder();

    var WW = $(window).width();

    function tabs(){
        var tab = $('.js-tab .tab__item'),
            tabCONTENT = $('.tab__content-item');

        tab.on('click',function(e) {
            e.preventDefault();
            $(this).parents('.tab').find(tab).removeClass('active');
            $(this).parents('.tab').find(tabCONTENT).removeClass('active');
            $(this).addClass('active');

            var eqTAB = $(this).index();
            $(this).parents('.tab').find(tabCONTENT).eq(eqTAB).addClass('active');
            dots();
        });
    }

    function pagination() {
        var item = $('.pagination__item');
        item.on('click',function (e) {
            e.preventDefault();
            var pagActive = $(this).text();
            $(this).parents('.pagination').attr('data-pagination',pagActive);
            $(this).parents('.pagination').find(item).removeClass('active');
            $(this).addClass('active');
        });

    }

    function tags() {
        var tag = $('.js-tags .tags__item');
        tag.on('click',function (e) {
            // e.preventDefault();
            $(this).parents('.js-tags').find(tag).removeClass('active');
            $(this).addClass('active');
        })
    }

    function tagsRemove() {
        var btn = $('.js-tags-remove .tags__del i'),
            btnRemove = $('.js-alltagsremove');
        btn.on('click',function (e) {
            e.preventDefault();
            $(this).parents('.tags__del').hide();
        });
        btnRemove.on('click',function (e) {
            e.preventDefault();
            $(this).parents('.js-tags-remove').slideUp();
        })
    }

    function tagsShow() {
        var btn = $('.js-showalltags');
        btn.on('click',function (e) {
            e.preventDefault();
            $(this).hide();
            $(this).prev('.js-tags').find('.tags__item.hidden').removeClass('hidden');
        });
    }

    function formStyler(){
        $('select').styler({
            selectSmartPositioning: true
        });

    }

    function textareaSize(){
        autosize($('textarea'));
    }

    function sliders(){
        $('#bg-slider').slick({
            dots: true,
            cssEase: 'linear',
            fade: true,
            speed: 800,
            arrows: false,
            asNavFor: '#main-slider'
        });

        $('#main-slider').slick({
            infinite: true,
            dots: false,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 50,
            fade: true,
            cssEase: 'linear',
            arrows: false,
            asNavFor: '#bg-slider'
        });

        $('.js-slider-arrows').on('click', function(e) {
            e.preventDefault();
            $('#main-slider').slick($(this).attr('data-slider'));
        });
    }

    function closePopup(){
        var popup = $('.popup'),
            popupItem = $('.popup__item');
        popup.removeClass('active');
        popupItem.removeClass('active');
    }

    function closePopupClick(){
        var btn = $('.js-close-popup');
        btn.on('click',function (e) {
            e.preventDefault();
            closePopup();
        });
    }

    function popupLogin(){
        var btn = $('.js-popup-login'),
            popupWrapper = $('.popup');
        btn.on('click',function (e) {
            e.preventDefault();
            $('.popup input').val('').removeClass('error');
            var popup = $(this).data('popup'),
                popupID = '#' + popup;
            popupWrapper.addClass('active');
            $(popupID).addClass('active');
        });
    }

    function validateLogin(){
        $('#validate-login').validate({
            rules: {
                USER_LOGIN: {
                    required: true
                },
                USER_PASSWORD: {
                    required: true
                }
            },
            messages: {
                USER_LOGIN: {
                    required: "Будь ласка, введіть електронну пошту"
                },
                USER_PASSWORD: {
                    required: "Будь ласка, введіть пароль"
                }
            }
        });
    }


    function popup(){
        var btn = $('.js-popup'),
            overlay = $('.overlay'),
            popup = $('.popup-window'),
            close = $('.js-popup-close');
        btn.on('click',function (e) {
            e.preventDefault();
            overlay.fadeIn(300);
            popup.addClass('active');
        });
        overlay.on('click',function (e) {
            e.preventDefault();
            overlay.fadeOut(300);
            popup.removeClass('active');
        });
        close.on('click',function (e) {
            e.preventDefault();
            overlay.fadeOut(300);
            popup.removeClass('active');
        });
    }

    function restorePass(){
        var btn = $('.js-restore-pass');
        btn.on('click',function (e) {
            e.preventDefault();
            $('#popup-login').removeClass('active');
            $('#popup-restore').addClass('active');
        })
    }

    function register(){
        var btn = $('.js-register');
        btn.on('click',function (e) {
            e.preventDefault();
            $('#popup-login').removeClass('active');
            $('#popup-register').addClass('active');
        })
    }

    function adaptiveMenu(){
        $('#nav-icon').on('click', function(e){
            e.preventDefault();
            $(this).toggleClass('open');
            $('.menu').toggleClass('active');
        });
    }

    function containerMenu(){
        var winWidth = $(window).width(),
            containerWidth = $('.container').eq(0).outerWidth(),
            width = ((winWidth - containerWidth)/2) + 15,
            menu = $('.menu'),
            userPic = $('.header__info-mobile');

        if (winWidth < 1201) {
            menu.css('padding-left',width);
            menu.css('padding-right',width);
        } else {
            menu.css('padding-left',0);
            menu.css('padding-right',0);
        }

        if (winWidth < 992) {
            userPic.css('padding-left',width);
            userPic.css('padding-right',width);
        } else {
            userPic.css('padding-left',0);
            userPic.css('padding-right',0);
        }
    }

    function adaptiveSearch(){
        var btn = $('.js-search-mobile');
        btn.on('click',function(e) {
            e.preventDefault();
            $('.header__search-input').val('');
            $('.header__search').show();
            $(this).hide();
        });
    }

    function mobileLng(){
        var btn = $('.js-mobile-lng');
        btn.on('click',function (e) {
            e.preventDefault();
            $('.mobile-lng__item').removeClass('active');
            $(this).addClass('active');
            var dataLNG = $(this).find('span').text();
            $('.mobile-lng').attr('data-lng',dataLNG);
        });
    }

    function seacrhClose(){
        var btn = $('.js-search-close');
        btn.on('click',function (e) {
            e.preventDefault();
            $('.header__search').hide();
            $('.js-search-mobile').show();
        });
    }

    function search(){
        var btn = $('.js-search');
        btn.on('click',function (e) {
            e.preventDefault();
            $('#search').submit();
        });
    }

    function research(){
        var btn = $('.js-research');
        btn.on('click',function (e) {
            e.preventDefault();
            $('#search-form-result').submit();
        });
    }
    function clearSearch(){
        var btn = $('.js-remove-result'),
            searchFieldResult = $('.search__result-field'),
            searchField = $('.search__result-form'),
            searchInput = $('.search__input-search');
        btn.on('click',function (e) {
            e.preventDefault();
            $(this).parents('.search__result-item').remove();
            if ($('.search__result-item').length == 0) {
                searchFieldResult.hide();
                searchField.show();
                searchInput.focus();
            }
        });
    }


    function mobileSubmenu(){
        if ($(window).width() < 1201) {
            var btn = $('.menu__link');
            btn.on('click',function (e) {
                e.preventDefault();
                if ($(this).parents('.menu__item').hasClass('active')) {
                    $('.menu__submenu').removeClass('active');
                } else {
                    $('.menu__submenu').removeClass('active');
                    $(this).parents('.menu__item').addClass('active');
                }
            });
        } else {
            $('.menu__submenu').removeClass('active');
        }
    }

    function mobileUsepic(){
        var btn = $('.js-user');
        btn.on('click',function (e) {
            // e.preventDefault();
            $(this).toggleClass('active');
        });
    }

    function logout(){
        var btn = $('.js-logout'),
            header = $('.header');
        btn.on('click',function (e) {
            e.preventDefault();
            if (header.hasClass('authorized')) {
                header.removeClass('authorized')
            }
        });
    }

    function fancy(){
        $('[data-fancybox="gallery"]').fancybox({
            buttons: [
                "zoom",
                //"share",
                // "slideShow",
                //"fullScreen",
                //"download",
                // "thumbs",
                "close"
            ],
            lang: "ua",
            i18n: {
                ua: {
                    CLOSE: "Закрити",
                    NEXT: "Наступне фото",
                    PREV: "Попереднє фото",
                    ERROR: "Запитаний вміст не можна завантажити. <br/> Будь-ласка спробуйте пізніше.",
                    PLAY_START: "Почати показ слайдів",
                    PLAY_STOP: "Призупинити показ слайдів",
                    FULL_SCREEN: "Повноекранний перегляд",
                    THUMBS: "Ескізи",
                    DOWNLOAD: "Завантажити",
                    SHARE: "Поділитися",
                    ZOOM: "Збільшення"
                },
                pl: {
                    CLOSE: 'Zamknij',
                    NEXT: 'Następny',
                    PREV: 'Poprzedni',
                    ERROR: 'Wystąpił błąd podczas ładowania zawartości.<br>Spróbuj ponownie.',
                    PLAY_START: 'Włącz automatyczne przeglądanie',
                    PLAY_STOP: 'Zatrzymaj automatyczne przeglądanie',
                    FULL_SCREEN: 'Pełny ekran',
                    THUMBS: 'Miniatury',
                    DOWNLOAD: 'Pobierz',
                    SHARE: 'Udostępnij',
                    ZOOM: 'Przybliż'
                }
            }
        });
    }

    function photoPreview(){
        var input = $('#newphoto');
        var preview = $('#photo-preview');
        var removeBtn = $('.js-delphoto');

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    preview.attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        input.change(function () {
            $('.addphoto__window').addClass('withimage');
            readURL(this);
        });

        removeBtn.on('click',function (e) {
            e.preventDefault();
            $('#newphoto').val('');
            $('.addphoto__window').removeClass('withimage');
            $('#photo-preview').attr('src','');
        });
    }

    function calendar(){
        var date=$("#datepicker").val();
        $('#datepicker').datepicker({
            showOn: "button",
            buttonImage: "/local/templates/mazuri/img/svg/calendar.svg",
            buttonImageOnly: true,
            buttonText: "Оберіть дату",
            dateFormat: 'mm/dd/yy'
        });
        $('#datepicker').datepicker("option",$.datepicker.regional["ua"]);
        if(date!=null)
        {
            $('#datepicker').datepicker('setDate', new Date(date));
        }

    }

    function sourceLink(){
        var btn = $('.source__list a');
        btn.on('click',function () {
            if ($(window).width() > 991) {
                var elementClick = $(this).attr("href");
                var destination = $(elementClick).offset().top;
                if ($(window).width() < 1201) {
                    var destination = $(elementClick).offset().top - 90;
                }
                jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 600);
                return false;
            }
        });
    }

    function sourceScrollMobile(){
        if ($(window).width() < 992) {
            var slider = $('.js-sourse-scroll'),
                sliderControl = $('.js-scroll-source');
            if (!$('.source__list').hasClass('slick-initialized')) {
                if($('.source__list').length>5){
                    slider.slick({
                        infinite: false,
                        arrows: false,
                        slidesToShow: 20,
                        slidesToScroll: 5,
                        responsive: [
                            {
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 15,
                                    slidesToScroll: 5,
                                }
                            },
                            {
                                breakpoint: 543,
                                settings: {
                                    slidesToShow: 9,
                                    slidesToScroll: 5,
                                }
                            }
                        ]
                    });}
            }
            sliderControl.on('click', function(e) {
                e.preventDefault();
                slider.slick($(this).attr('data-slider'));
            });
            var sourceLetter = $('.source__list a'),
                sourceTable = $('.source__table');
            sourceLetter.removeClass('active');
            sourceTable.removeClass('active');
            sourceLetter.eq(0).addClass('active');
            sourceTable.eq(0).addClass('active');
            sourceLetter.on('click',function (e) {
                e.preventDefault();
                sourceLetter.removeClass('active');
                $(this).addClass('active');
                sourceTable.removeClass('active');
                var tab = $(this).attr('href');
                $(tab).addClass('active');
            });
        } else {
            if ($('.source__list').hasClass('slick-initialized')) {
                $('.js-sourse-scroll').slick('unslick');
            }
            $('.source__list a').removeClass('active');
            $('.source__table').removeClass('active');
        }
    }

    function categoryTabMobile() {
        var WW = $(window).width(),
            btn = $('.cabinet__tabs .tab__content-item');
        if (WW < 544) {
            btn.removeClass('active');
            btn.on('click',function () {
                btn.removeClass('active');
                $(this).addClass('active');
            });
        }
    }

    function telMask(){
        $('.js-mask').mask('+38 000 000 00 00');
    }

    function dots(){
        $( ".stories__text" ).dotdotdot();
    }

    function sort(){
        var btn = $('.js-sort .page-sort__btn');
        btn.on('click', function (e) {
            e.preventDefault();
            btn.removeClass('active');
            $(this).addClass('active');
        })
    }

    sliders();
    formStyler();
    textareaSize();

    $(document).ready(function() {
        tabs();
        //pagination();
        tags();
        tagsRemove();
        tagsShow();
        popup();
        popupLogin();
        validateLogin();
        restorePass();
        register();
        closePopupClick();
        adaptiveMenu();
        containerMenu();
        adaptiveSearch();
        seacrhClose();
        mobileLng();
        search();
        mobileSubmenu();
        mobileUsepic();
        logout();
        fancy();
        photoPreview();
        calendar();
        clearSearch();
        sourceLink();
        sourceScrollMobile();
        research();
        telMask();
        categoryTabMobile();
        dots();
        sort();
    });

    $(window).on("load",function(){
        $(".scroll").mCustomScrollbar();
    });

    $(window).on("resize",function(){
        containerMenu();
        sourceScrollMobile();
        categoryTabMobile();
    });
});

//функция для показа попапа
var showPopup = function(title, text){
    $('.popup-window h2.title').html(title);
    $('.popup-window .text-body').html(text);
    $('.overlay').fadeIn(300);
    $('.popup-window').addClass('active');
};